<?php

namespace Webspeaks\BannerSlider\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * @param \Magento\Framework\App\Helper\Context   $context
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl
    ) {
        parent::__construct($context);
        $this->_backendUrl = $backendUrl;
    }


    /**
     * get Slider Slides Tab Url
     * @return string
     */
    public function getSliderSlidesUrl()
    {
        return $this->_backendUrl->getUrl('wsslider/slider/slides', ['_current' => true]);
    }

}
