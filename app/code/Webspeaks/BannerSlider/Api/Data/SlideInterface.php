<?php
namespace Webspeaks\BannerSlider\Api\Data;

interface SlideInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const SLIDE_ID      = 'slide_id';
    const SLIDE_TITLE   = 'slide_title';
    const SLIDE_ALT     = 'slide_alt';
    const CONTENT       = 'content';
    const OPTIONS       = 'options';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    const IS_ACTIVE     = 'is_active';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Slide title
     *
     * @return string
     */
    public function getSlideTitle();

    /**
     * Get Slide alt text
     *
     * @return string
     */
    public function getSlideAlt();

    /**
     * Get content
     *
     * @return string
     */
    public function getContent();

    /**
     * Get options
     *
     * @return string|null
     */
    public function getOptions();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setId($id);

    /**
     * Set Slide title
     *
     * @param string $slide_title
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setSlideTitle($slide_title);

    /**
     * Set Slide Alt text
     *
     * @param string $slide_title
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setSlideAlt($slide_alt);

    /**
     * Set content
     *
     * @param string $content
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setContent($content);

    /**
     * Set options
     *
     * @param string $options
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setOtions($options);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setIsActive($isActive);
}