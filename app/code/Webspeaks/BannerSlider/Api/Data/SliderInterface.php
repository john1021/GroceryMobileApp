<?php
namespace Webspeaks\BannerSlider\Api\Data;

interface SliderInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const SLIDER_ID     = 'slider_id';
    const SLIDER_NAME   = 'slider_name';
    const SLUG          = 'slug';
    const SLIDES        = 'slides';
    const OPTIONS       = 'options';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    const IS_ACTIVE     = 'is_active';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Slider Name
     *
     * @return string
     */
    public function getSliderName();

    /**
     * Get Slug
     *
     * @return string
     */
    public function getSlug();

    /**
     * Get Slides
     *
     * @return string|null
     */
    public function getSlides();

    /**
     * Get options
     *
     * @return string|null
     */
    public function getOptions();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setId($id);

    /**
     * Set Slider name
     *
     * @param string $slider_name
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setSliderName($slider_name);

    /**
     * Set slug
     *
     * @param string $slug
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setSlug($slug);

    /**
     * Set title
     *
     * @param string $slides
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setSlides($slides);

    /**
     * Set options
     *
     * @param string $options
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setOtions($options);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setIsActive($isActive);
}