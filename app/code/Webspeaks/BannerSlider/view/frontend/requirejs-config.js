var config = {
	"map": {
	    "*": {
	    	"bxslider": "js/bxslider/jquery.bxslider.min"
	    }
	},
	paths: {
		'webspeaks/bxslider': 'Webspeaks_BannerSlider/js/bxslider/jquery.bxslider.min'
	},
	shim: {
		'webspeaks/bxslider': {
			deps: ['jquery']
		}
	}
};
