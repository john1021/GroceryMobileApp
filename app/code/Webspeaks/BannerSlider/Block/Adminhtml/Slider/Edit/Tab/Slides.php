<?php

namespace Webspeaks\BannerSlider\Block\Adminhtml\Slider\Edit\Tab;

class Slides extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * Slide factory
     *
     * @var \Webspeaks\BannerSlider\Model\ResourceModel\Slide\CollectionFactory
     */
    protected $_slideCollectionFactory;

    protected $_objectManager = null;

    /**
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Webspeaks\BannerSlider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Webspeaks\BannerSlider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        array $data = []
    ) {
        $this->_slideCollectionFactory = $slideCollectionFactory;
        $this->_objectManager = $objectManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * _construct
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('slideGrid');
        $this->setDefaultSort('slide_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('slider_id')) {
            $this->setDefaultFilter(array('in_slide' => 1));
        }
    }

    /**
     * add Column Filter To Collection
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_slide') {
            $slideIds = $this->_getSelectedSlides();

            if (empty($slideIds)) {
                $slideIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('slide_id', array('in' => $slideIds));
            } else {
                if ($slideIds) {
                    $this->getCollection()->addFieldToFilter('slide_id', array('nin' => $slideIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * prepare collection
     */
    protected function _prepareCollection()
    {
        /** @var \Webspeaks\BannerSlider\Model\ResourceModel\Slide\Collection $collection */
        $collection = $this->_slideCollectionFactory->create();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        /* @var $model \Webspeaks\BannerSlider\Model\Slide */
        $model = $this->_objectManager->get('\Webspeaks\BannerSlider\Model\Slide');

        $this->addColumn(
            'in_slide',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_slide',
                'align' => 'center',
                'index' => 'slide_id',
                'values' => $this->_getSelectedSlides(),
            ]
        );

        $this->addColumn(
            'slide_id',
            [
                'header' => __('Slide ID'),
                'type' => 'number',
                'index' => 'slide_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'slide_title',
            [
                'header' => __('Title'),
                'index' => 'slide_title',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );

        $this->addColumn(
            'preview',
            [
                'header' => __('Preview'),
                'filter' => false,
                'sortable' => false,
                'renderer' => 'Webspeaks\BannerSlider\Block\Adminhtml\Slider\Edit\Tab\Renderer\SlidePreview',
            ]
        );

        $this->addColumn(
            'slide_is_active',
            [
                'header' => __('Status'),
                'index' => 'is_active',
                'type' => 'options',
                'filter_index' => 'main_table.is_active',
                'options' => $model->getAvailableStatuses(),
            ]
        );

        $this->addColumn(
            'start_time',
            [
                'header' => __('Start time'),
                'type' => 'datetime',
                'index' => 'start_time',
                'timezone' => true,
            ]
        );

        $this->addColumn(
            'end_time',
            [
                'header' => __('End time'),
                'type' => 'datetime',
                'index' => 'end_time',
                'timezone' => true,
            ]
        );

        $this->addColumn(
            'slide_order',
            [
                'header' => __('Order'),
                'name' => 'slide_order',
                'index' => 'order',
                'class' => 'xxx',
                'width' => '50px',
                'editable' => true,
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/slidesgrid', ['_current' => true]);
    }

    /**
     * @param  object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }

    public function getSelectedSliderSlides()
    {
        $sliderId = $this->getRequest()->getParam('slider_id');
        if (!isset($sliderId)) {
            return [];
        }
        $slideCollection = $this->_slideCollectionFactory->create();
        $slideCollection->addFieldToFilter('slider_id', $sliderId);

        $slideIds = [];
        foreach ($slideCollection as $slide) {
            $slideIds[$slide->getId()] = ['slide_order' => $slide->getOrder()];
        }

        return $slideIds;
    }

    protected function _getSelectedSlides()
    {
        $slides = $this->getRequest()->getParam('slide');
        if (!is_array($slides)) {
            $slides = array_keys($this->getSelectedSliderSlides());
        }

        return $slides;
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return true;
    }
}
