<?php

namespace Webspeaks\BannerSlider\Block\Adminhtml\Slider\Edit\Tab\Renderer;

class SlidePreview extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * Store manager.
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Slice factory
     *
     * @var \Webspeaks\BannerSlider\Model\SlideFactory
     */
    protected $_slideFactory;

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     *
     * @param \Magento\Backend\Block\Context              $context
     * @param \Magento\Store\Model\StoreManagerInterface  $storeManager
     * @param \Webspeaks\BannerSlider\Model\SlideFactory  $slideFactory
     * @param \Magento\Cms\Model\Template\FilterProvider  $filterProvider
     * @param array                                       $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webspeaks\BannerSlider\Model\SlideFactory $slideFactory,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_storeManager = $storeManager;
        $this->_slideFactory = $slideFactory;
        $this->_filterProvider = $filterProvider;
    }

    /**
     *
     * @param \Magento\Framework\DataObject $row
     *
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
    	$_slide = $this->_slideFactory->create()->load($row->getId());
    	$html = $this->_filterProvider->getPageFilter()->filter($_slide->getContent());
    	$output = '<div style="max-height:100px;max-width:100px;overflow:hidden">' . $html . '</div>';
        $output .= '<br><a href="' . $this->getUrl('*/slide/edit', ['_current' => FALSE, 'slide_id' => $row->getId()]) . '" target="_blank">Edit</a> ';
        return $output;
    }
}
