<?php
namespace Webspeaks\BannerSlider\Block\Widget;
 
class BannerSlider extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{

    protected $_template = 'widget/banner_slider.phtml';

    /**
     * stdlib timezone.
     *
     * @var \Magento\Framework\Stdlib\DateTime\Timezone
     */
    protected $_stdTimezone;

    /**
     * @var \Webspeaks\BannerSlider\Model\sliderFactory
     */
    protected $_sliderFactory;

	/**
     * @var \Webspeaks\BannerSlider\Model\slideFactory
     */
    protected $_slideFactory;

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     * @param \Magento\Framework\Stdlib\DateTime\Timezone $_stdTimezone
 */
    protected $_filterProvider;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webspeaks\BannerSlider\Model\SlideFactory $slideFactory,
        \Webspeaks\BannerSlider\Model\SliderFactory $sliderFactory,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Framework\Stdlib\DateTime\Timezone $_stdTimezone
    ) {
        parent::__construct($context);
        $this->_sliderFactory = $sliderFactory;
        $this->_slideFactory = $slideFactory;
        $this->_filterProvider = $filterProvider;
        $this->_stdTimezone = $_stdTimezone;
    }

    public function getSliderInfo($slider_id=null)
    {
        if (empty($slider_id)) {
            return null;
        }

        return $this->_sliderFactory->create()->load($slider_id);
    }

    public function getSlidesFromSlider($slider_id=null)
    {
    	if (empty($slider_id)) {
    		return null;
    	}

        $now = $this->_stdTimezone->date()->format('Y-m-d H:i:s');

    	$collection = $this->_slideFactory->create()->getCollection()
    					->addFieldToFilter('slider_id', $slider_id)
    					->addFieldToFilter('is_active', 1)
                        ->addFieldToFilter('start_time', ['lteq' => $now])
                        ->addFieldToFilter('end_time', ['gteq' => $now])
                        ->setOrder('main_table.order', 'ASC')
    					;
    	return $collection;
    }

    public function getSlideHtml($_slide)
    {
    	$html = $this->_filterProvider->getPageFilter()->filter($_slide->getContent());
    	return $html;
    }

    public function getSliderId()
    {
    	return 'ws-bx-slider-' . mt_rand();
    }
}