<?php
namespace Webspeaks\BannerSlider\Model\Slider\Source;

class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Webspeaks\BannerSlider\Model\Slider
     */
    protected $slider;

    /**
     * Constructor
     *
     * @param \Webspeaks\BannerSlider\Model\Slider $slider
     */
    public function __construct(\Webspeaks\BannerSlider\Model\Slider $slider)
    {
        $this->slider = $slider;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->slider->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}