<?php
namespace Webspeaks\BannerSlider\Model;

use Webspeaks\BannerSlider\Api\Data\SlideInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Slide extends \Magento\Framework\Model\AbstractModel implements SlideInterface, IdentityInterface
{

    /**#@+
     * Slider's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'ws_slide';

    /**
     * @var string
     */
    protected $_cacheTag = 'ws_slide';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ws_slide';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webspeaks\BannerSlider\Model\ResourceModel\Slide');
    }

    /**
     * Prepare slide's statuses.
     * Available event slide_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::SLIDE_ID);
    }

    /**
     * Get Slide title
     *
     * @return string
     */
    public function getSlideTitle()
    {
        return $this->getData(self::SLIDE_TITLE);
    }

    /**
     * Get Slide alt text
     *
     * @return string
     */
    public function getSlideAlt()
    {
        return $this->getData(self::SLIDE_ALT);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get options
     *
     * @return string|null
     */
    public function getOptions()
    {
        return $this->getData(self::OPTIONS);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setId($id)
    {
        return $this->setData(self::SLIDE_ID, $id);
    }

    /**
     * Set Slide title
     *
     * @param string $slide_title
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setSlideTitle($slide_title)
    {
        return $this->setData(self::SLIDE_TITLE, $slide_title);
    }
    /**
     * Set Slide alt text
     *
     * @param string $slide_alt
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setSlideAlt($slide_alt)
    {
        return $this->setData(self::SLIDE_ALT, $slide_alt);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set options
     *
     * @param string $options
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setOtions($options)
    {
        return $this->setData(self::OPTIONS, $options);
    }

    /**
     * Set creation time
     *
     * @param string $creation_time
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * Set update time
     *
     * @param string $update_time
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     * @return \Webspeaks\BannerSlider\Api\Data\SlideInterface
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }

}