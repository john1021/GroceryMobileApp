<?php

namespace Webspeaks\BannerSlider\Model\Config;

class Sliders implements \Magento\Framework\Option\ArrayInterface
{

	 /**
     * @var \Webspeaks\BannerSlider\Model\sliderFactory
     */
    protected $_sliderFactory;
    
     /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Webspeaks\BannerSlider\Model\SliderFactory $sliderFactory
     * @param \Webspeaks\BannerSlider\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Webspeaks\BannerSlider\Model\SliderFactory $sliderFactory
    ) {
        $this->_sliderFactory = $sliderFactory;
    }
    
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
    	$optionArray = array();
    	foreach($this->toArray() as $key => $val){
    		$optionArray[] = array( 'value' => $key , 'label' => $val);
    	}
        return $optionArray;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
    	$group = [];
    	$collection = $this->_sliderFactory->create()->getCollection();
    	
    	foreach($collection as $slider) {
    		$group[$slider->getId()] = $slider->getSliderName();
    	}
        return $group;
    }
}