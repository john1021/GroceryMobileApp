<?php
namespace Webspeaks\BannerSlider\Model\Slide\Source;

class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Webspeaks\BannerSlider\Model\Slide
     */
    protected $slide;

    /**
     * Constructor
     *
     * @param \Webspeaks\BannerSlider\Model\Slide $slide
     */
    public function __construct(\Webspeaks\BannerSlider\Model\Slide $slide)
    {
        $this->slide = $slide;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->slide->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}