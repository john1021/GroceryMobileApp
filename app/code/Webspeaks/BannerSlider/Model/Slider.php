<?php
namespace Webspeaks\BannerSlider\Model;

use Webspeaks\BannerSlider\Api\Data\SliderInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Slider extends \Magento\Framework\Model\AbstractModel implements SliderInterface, IdentityInterface
{

    /**#@+
     * Slider's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'ws_slider';

    /**
     * @var string
     */
    protected $_cacheTag = 'ws_slider';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ws_slider';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webspeaks\BannerSlider\Model\ResourceModel\Slider');
    }

    /**
     * Prepare slider's statuses.
     * Available event slider_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::SLIDER_ID);
    }

    /**
     * Get Slider Name
     *
     * @return string
     */
    public function getSliderName()
    {
        return $this->getData(self::SLIDER_NAME);
    }

    /**
     * Get Slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->getData(self::SLUG);
    }

    /**
     * Get slides
     *
     * @return string|null
     */
    public function getSlides()
    {
        return $this->getData(self::SLIDES);
    }

    /**
     * Get options
     *
     * @return string|null
     */
    public function getOptions()
    {
        return $this->getData(self::OPTIONS);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setId($id)
    {
        return $this->setData(self::SLIDER_ID, $id);
    }

    /**
     * Set Slider Name
     *
     * @param string $slider_name
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setSliderName($slider_name)
    {
        return $this->setData(self::SLIDER_NAME, $slider_name);
    }

    /**
     * Set Slug
     *
     * @param string $slug
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setSlug($slug)
    {
        return $this->setData(self::SLUG, $slug);
    }

    /**
     * Set slides
     *
     * @param string $slides
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setSlides($slides)
    {
        return $this->setData(self::SLIDES, $title);
    }

    /**
     * Set options
     *
     * @param string $options
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setOtions($options)
    {
        return $this->setData(self::OPTIONS, $options);
    }

    /**
     * Set creation time
     *
     * @param string $creation_time
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * Set update time
     *
     * @param string $update_time
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     * @return \Webspeaks\BannerSlider\Api\Data\SliderInterface
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }

    /**
     * get availabe slide.
     *
     * @return []
     */
    public function getAvailableSliders()
    {

        $options = [];
        $sliderCollection = $this->getResourceCollection()
                            ->addFieldToFilter('is_active', 1);
        foreach ($sliderCollection as $slider) {
            $options[] = [
                'value' => $slider->getId(),
                'label' => $slider->getSliderName(),
                'level' => 1,
            ];
        }

        return $options;
    }

}