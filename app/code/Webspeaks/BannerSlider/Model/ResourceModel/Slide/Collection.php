<?php
namespace Webspeaks\BannerSlider\Model\ResourceModel\Slide;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'slide_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webspeaks\BannerSlider\Model\Slide', 'Webspeaks\BannerSlider\Model\ResourceModel\Slide');
    }

}