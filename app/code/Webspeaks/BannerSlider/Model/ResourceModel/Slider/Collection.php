<?php
namespace Webspeaks\BannerSlider\Model\ResourceModel\Slider;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'slider_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webspeaks\BannerSlider\Model\Slider', 'Webspeaks\BannerSlider\Model\ResourceModel\Slider');
    }

}