<?php 
namespace Webspeaks\BannerSlider\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('ws_slider'))
            ->addColumn(
                'slider_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Slider ID'
            )
            ->addColumn('slug', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('slider_name', Table::TYPE_TEXT, 255, ['nullable' => false], 'Slider Name')
            ->addColumn('options', Table::TYPE_TEXT, '2M', [], 'Slider Options')
            ->addColumn('is_active', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '1'], 'Is Slide Active?')
            ->addColumn('creation_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Creation Time')
            ->addColumn('update_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Update Time')
            ->addIndex($installer->getIdxName('slider_slug', ['slug']), ['slug'])
            ->setComment('Webspeaks Slider');

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()
            ->newTable($installer->getTable('ws_slide'))
            ->addColumn(
                'slide_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Slide ID'
            )
            ->addColumn('slide_title', Table::TYPE_TEXT, 255, ['nullable' => false], 'Slide Title')
            ->addColumn('content', Table::TYPE_TEXT, '2M', ['nullable' => false], 'Slide Content')
            ->addColumn('options', Table::TYPE_TEXT, '2M', [], 'Slide Options')
            ->addColumn('slider_id', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => 0], 'Attached slider id')
            ->addColumn('is_active', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '1'], 'Is Slide Active?')
            ->addColumn(
                'start_time',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true],
                'Slider start time'
            )->addColumn(
                'end_time',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                ['nullable' => true],
                'Slider end time'
            )
            ->addColumn('creation_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Creation Time')
            ->addColumn('update_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Update Time')
            ->setComment('Webspeaks Slide');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}