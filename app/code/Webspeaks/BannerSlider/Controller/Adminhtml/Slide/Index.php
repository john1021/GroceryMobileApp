<?php
namespace Webspeaks\BannerSlider\Controller\Adminhtml\Slide;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Webspeaks_BannerSlider::slide');
        $resultPage->addBreadcrumb(__('Slides'), __('Slides'));
        $resultPage->addBreadcrumb(__('Manage Slides'), __('Manage Slides'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Slides'));

        return $resultPage;
    }

    /**
     * Is the user allowed to view the banner slider grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webspeaks_BannerSlider::slider');
    }


}