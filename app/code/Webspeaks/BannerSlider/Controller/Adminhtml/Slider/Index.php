<?php
namespace Webspeaks\BannerSlider\Controller\Adminhtml\Slider;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Webspeaks_BannerSlider::slider');
        $resultPage->addBreadcrumb(__('Banner Sliders'), __('Banner Sliders'));
        $resultPage->addBreadcrumb(__('Manage Blog Posts'), __('Manage Sliders'));
        $resultPage->getConfig()->getTitle()->prepend(__('Banner Slider'));

        return $resultPage;
    }

    /**
     * Is the user allowed to view the banner slider grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webspeaks_BannerSlider::slider');
    }


}