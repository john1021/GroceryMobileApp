<?php
namespace Webspeaks\BannerSlider\Controller\Adminhtml\Slider;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Save extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $_jsHelper;

    /**
     * @var \Webspeaks\BannerSlider\Model\ResourceModel\Slide\CollectionFactory
     */
    protected $_slideCollectionFactory;

    /**
     * \Magento\Backend\Helper\Js $jsHelper
     * @param Action\Context $context
     */
    public function __construct(
        Action\Context $context,
        \Magento\Backend\Helper\Js $jsHelper,
        \Webspeaks\BannerSlider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory
    ) {
        parent::__construct($context);
        $this->_jsHelper = $jsHelper;
        $this->_slideCollectionFactory = $slideCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webspeaks_BannerSlider::save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            // var_dump($data);die();

            /** @var \Webspeaks\BannerSlider\Model\Slider $model */
            $model = $this->_objectManager->create('Webspeaks\BannerSlider\Model\Slider');

            $id = $this->getRequest()->getParam('slider_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            $this->_eventManager->dispatch(
                'wsbanner_slider_prepare_save',
                ['post' => $model, 'request' => $this->getRequest()]
            );

            try {
                $model->save();

                // Attach the slides to slider
                if (isset($data['slider_slide'])) {
                    $slidesSerialized = $this->_jsHelper->decodeGridSerializedInput($data['slider_slide']);

                    // var_dump($slidesSerialized);die();
                    $slideIds = array_keys($slidesSerialized);
                    $slideOrder = [];
                    foreach ($slidesSerialized as $_sid => $_slide) {
                        $slideOrder[$_sid] = $_slide['slide_order'];
                    }
                    // var_dump($slideOrder);die();

                    $removeSlidesCollection = $this->_slideCollectionFactory
                                        ->create()
                                        ->addFieldToFilter('slider_id', $model->getId());
                    if (count($slideIds)) {
                        $removeSlidesCollection->addFieldToFilter('slide_id', array('nin' => $slideIds));
                    }

                    foreach ($removeSlidesCollection as $_slide) {
                        $_slide->setSliderId(0)
                            ->setStoreViewId(null)
                            ->save();
                    }

                    // Attach slides to slider
                    $slidesCollection = $this->_slideCollectionFactory
                                        ->create()
                                        ->addFieldToFilter('slide_id', array('in' => $slideIds));
                    foreach ($slidesCollection as $_slide) {
                        $_slide->setSliderId($model->getId())
                            ->setStoreViewId(null)
                            ->setOrder($slideOrder[$_slide->getId()])
                            ->save();
                    }

                }

                // var_dump($data);
                // die();


                $this->messageManager->addSuccess(__('You saved this Slider.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['slider_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the slider.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['slider_id' => $this->getRequest()->getParam('slider_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}