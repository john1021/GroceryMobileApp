<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Addcartcustom;
class Index extends \Magento\Framework\App\Action\Action
{
	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;
    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;
    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
	    $post = $this->getRequest()->getPostValue();
		$product_sku_id=$_POST['sku_id'];
		$customer_id=$_POST['customer_id'];
	  
	  /*Starts For Admin Login Process*/
	    $adminUrl='http://18.219.227.98/index.php/rest/V1/integration/admin/token';
	
		$ch = curl_init();
	
        $data = array("username" => "admin", "password" => "admin123!@#$");	
		
		$data_string = json_encode($data);                       
		$ch = curl_init($adminUrl); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);
	    $token_admin = curl_exec($ch);
		$token_admin=  json_decode($token_admin); 
		$headers_admin = array("Authorization: Bearer ".$token_admin);
		
	 /*Finish For Admin Login Process*/	
	
	 /*Starts For Customer Login Process*/
	
	   $adminUrl='http://18.219.227.98/index.php/rest/V1/integration/customer/token';
		$ch = curl_init();
		$data = array("username" => "raj.k230@gmail.com", "password" => "admin123!!");   
		
		$data_string = json_encode($data);                       
		$ch = curl_init($adminUrl); 
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);       

		$token = curl_exec($ch);
		echo"<BR>Customer Token==".$token=  json_decode($token); 
		$headers = array("Authorization: Bearer ".$token);
			 
		$requestUrl='http://18.219.227.98/index.php/rest/V1/carts/mine';
		
		$ch = curl_init($requestUrl);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		echo"<BR>==".$result=  json_decode($result);

		
	 /*Finish For Customer Login Process*/
	    $productData = [
		'cart_item' =>	[
			   'quote_id' => 113,
				'sku' => "pro123",
				'qty' => 1
			]
			
    
		];
	 /*Starts Cart Empty Process*/

	
	$ch = curl_init("http://18.219.227.98/index.php/rest/V1/carts/mine/items");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

	$result = curl_exec($ch);
	$result = json_decode($result, 1);
	
	
	
	
	//exit();
	if(empty($result)) {
	$data[]=array('error'=>'true','message'=>'Currency not found','status_code'=>'404','data'=>$result);
	} else {
	$data[]=array('error'=>'false','message'=>'Cart Details','status_code'=>'200','data'=>$result);
	}

	unset($data['username']);
	unset($data['password']); 

	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	

			
        
    }
}
