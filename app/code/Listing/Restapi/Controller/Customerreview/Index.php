<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Customerreview;

class Index extends \Magento\Framework\App\Action\Action
{
	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;
    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;
    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		
		
		$post = $this->getRequest()->getPostValue();
	 
		 //print_r($post);
		// exit();

		$user_id=$_POST['user_id'];
		$deliverboy_rating=$_POST['deliverboy_rating'];
		$grocery_rating=$_POST['grocery_rating'];
		$feedback_msg=$_POST['feedback_msg'];
		
	
		//print_r($post);
		//exit();
		
		
		/*Starts Fetch User Result */
		
	   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
	   $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	   $connection = $resource->getConnection();
	  
 	   $tableName1 = $resource->getTableName('customer_entity'); //gives table name with prefix
	   
	   $tableName2 = $resource->getTableName('review'); //gives table name with prefix

	   $sql = "select * 
			 from $tableName1 t1
			 where t1.entity_id=$user_id";
	   $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
	   
	  

		$total=count($result);
		
	 /*Finish Fetch User Result */	

	    if($total=1) {
	   $data[]=array('error'=>'false','message'=>"Customer Review Detail",'status_code'=>'200','data'=>$post);
        } else {
		$data[]=array('error'=>'true','message'=>"Please fill required fields correct format",'status_code'=>'404');	
		}	
		
		
	unset($data['username']);
	unset($data['password']); 
	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	
		
	  
    }
	
	
	
	
	
}
