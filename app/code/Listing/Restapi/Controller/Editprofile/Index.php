<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Editprofile;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
 public function execute()
 
    {
		
	//Authentication rest API magento2.Please change url accordingly your url
	//echo"<BR>Testing";
	 //exit();
		
	 $post = $this->getRequest()->getPostValue();

	 $cust_username=$_POST['cust_username'];
	 $cust_firstname=$_POST['cust_firstname'];
	 $cust_lastname=$_POST['cust_lastname'];
	 $cust_phoneno=$_POST['cust_phoneno'];
	 $cust_address=$_POST['cust_address'];
	 $cust_id=$_POST['cust_id'];
	 
	  //print_r($_POST);
	  //exit();
	
	 $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
	 $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	 $connection = $resource->getConnection();
	 $tableName = $resource->getTableName('customer_entity'); //gives table name with prefix
	
	
	 /*Starts Process For Profile Updation*/
	 $sql = "Update " . $tableName . " Set firstname='$cust_firstname', lastname='$cust_lastname', cust_phone=$cust_phoneno,cust_address='$cust_address'  where email = '$cust_username' OR entity_id = $cust_id";
	 // print_r($_POST);
	  //exit();
	 $connection->query($sql);
	 /*Finish Process For Profile Updation*/
	 
	 $sql = "Select email, firstname, lastname, cust_phone, cust_address FROM ". $tableName." Where email = '$cust_username' OR entity_id = $cust_id";
     $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
	 
	//print_r($result);
    //exit();
    // $result=  json_decode($result)
	 
	  if($connection->query($sql)){
		  $data[]=array('error'=>'false','message'=>'Your profile has been updated successfully','status_code'=>'200','data'=>$result);
		 } else {
		  $data[]=array('error'=>'false','message'=>'Please fill all required field','status_code'=>'200');
         }	 
	

	   unset($data['username']);
	   unset($data['password']); 

	   echo json_encode($data[0], JSON_UNESCAPED_SLASHES);
		
	}
	
}
