<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Productsearch;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
    /*Starts Process For Base URL */
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $baseUrl = $storeManager->getStore()->getBaseUrl();	
	/*Finish Process For Base URL */
	$post = $this->getRequest()->getPostValue();
	//$cat_id=$product_sku=$this->getRequest()->getParam('id');
	
	$cat_id=$_POST['Search'];
	
	
	/*Starts Process For Category Name */
   /* $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $object_manager = $_objectManager->create('Magento\Catalog\Model\Category')->load($cat_id);
	$data[]=array();
	$data=$object_manager->getData();
	$data_count=count($data);
	$cat_name=$data['name'];*/
	//exit();
    /*Finish Process For Category Name */
	
	$adminUrl="$baseUrl"."index.php/rest/V1/integration/admin/token";
	
	$ch = curl_init();
	//$data = array("username" => "Raj", "password" => "mani123!!!");   
    $data = array("username" => "admin", "password" => "admin123!@#$");	
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);

	$token=  json_decode($token); 
	
	$headers = array("Authorization: Bearer $token"); 
	
	//$requestUrl="$baseUrl"."index.php/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]=".$cat_id;

	/*$requestUrl="$baseUrl"."index.php/rest/V1/products?
	searchCriteria[filter_groups][0][filters][0][field]=name&
	searchCriteria[filter_groups][0][filters][0][value]="."%"."$cat_id"."%"."&
	searchCriteria[filter_groups][0][filters][0][condition_type]=like";*/
	
	
	//$requestUrl="$baseUrl"."index.php/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]=".$cat_id."&searchCriteria[filter_groups][0][filters][0][condition_type]=like";

	
$requestUrl="$baseUrl"."index.php/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]="."%25".$cat_id."%25"."&searchCriteria[filter_groups][0][filters][0][condition_type]=like";

    $ch = curl_init();
	$ch = curl_init($requestUrl);	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	$result = curl_exec($ch);
	$result=  json_decode($result);	

	//$data[]=array('error'=>'false','message'=>"$cat_name",'status_code'=>'200','data'=>$result);
	

	//if($data_count==29){
	$data[]=array('error'=>'false','message'=>"Product Details",'status_code'=>'200','data'=>$result);
	//}else{
	//$data[]=array('error'=>'true','message'=>"Please provide correct Category ID",'status_code'=>'400');	
	//}	
		
		unset($data['username']);
	    unset($data['password']); 

	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);		

		/* 
		echo"<pre>";
		print_r($result);
		echo"</pre>";
		*/
		
    }
}
