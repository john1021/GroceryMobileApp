<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Registration;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
 public function execute()
    {
		//Authentication rest API magento2.Please change url accordingly your url
		
	 $post = $this->getRequest()->getPostValue();
	 
	 //print_r($post);
	// exit();

	$cust_username=$_POST['cust_username'];
	$cust_firstname=$_POST['cust_firstname'];
	$cust_lastname=$_POST['cust_lastname'];
	$cust_phoneno=$_POST['cust_phoneno'];
	$cust_address=$_POST['cust_address'];
	$cust_pass=$_POST['cust_pass'];
	$cust_confirmpass=$_POST['cust_confirmpass'];

		
	  $customerData = [
        'customer' => [
            "email" => "$cust_username",
            "firstname" => "$cust_firstname",
            "lastname" => "$cust_lastname",
            "storeId" => 1,
            "websiteId" => 1
        ],
        "password" => "$cust_pass"
    ];	
		

	
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $baseUrl = $storeManager->getStore()->getBaseUrl();	
	
	$adminUrl="$baseUrl"."index.php/rest/V1/integration/admin/token";
	
	$ch = curl_init();
	$data = array("username" => "Raj", "password" => "mani123!!!");                                                                    
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);
	$token=  json_decode($token); 
		
	$requestUrl="$baseUrl"."index.php/rest/V1/customers";	


    $email_id=$cust_username;

    $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
        ->get('Magento\Framework\App\ResourceConnection');
    $connection= $this->_resources->getConnection();
 
    $select = $connection->select()
        ->from(
            ['o' =>  $this->_resources->getTableName('customer_entity')]
        )->where('o.email=?',$email_id);
 
    $result = $connection->fetchAll ($select);
    $total=count($result);
	
    if($cust_confirmpass==$cust_pass) {
	
    if($total==1) {
		   
		   $ch = curl_init();
	$ch = curl_init($requestUrl);	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   //Change
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

	$result = curl_exec($ch);
	$result=  json_decode($result);	

		   
	$data[]=array('error'=>'true','message'=>'A customer with the same email already exists in an associated website.','status_code'=>'404','data'=>$result);
	
	} else {
		
		
	
		
			
	$ch = curl_init();
	$ch = curl_init($requestUrl);	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   //Change
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

	$result = curl_exec($ch);
	$result=  json_decode($result);	
	
	
		/*Starst Query To Update Phone & Address */
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('customer_entity'); //gives table name with prefix
		
		$sql = "Update " . $tableName . " Set cust_phone = $cust_phoneno, cust_address = '$cust_address' where email = '$cust_username'";
		$connection->query($sql);
		/*Starst Query To Update Phone & Address */	
	
	/*Starts Fetch User Result */
		
	   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
	   $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	   $connection = $resource->getConnection();
	   $tableName = $resource->getTableName('customer_entity'); //gives table name with prefix

	   $sql = "Select entity_id,email, firstname, lastname, cust_phone, cust_address, session_status FROM ". $tableName." Where email = '$cust_username'";
	   $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
		
	/*Finish Fetch User Result */	

	
	$data[]=array('error'=>'false','message'=>'Registration has been done successfully, User Credentails has beeen sent to your registered email','status_code'=>'200','data'=>$result);
	
	
	
	
	} } else {
		
	$data[]=array('error'=>'true','message'=>'Confirm password not same as password.','status_code'=>'404');
		
	}	
	
	

	unset($data['username']);
	unset($data['password']); 

	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	

    }
}
