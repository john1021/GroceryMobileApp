<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Login;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
 public function execute()
    {

	    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl = $storeManager->getStore()->getBaseUrl();	
	
	    $post = $this->getRequest()->getPostValue();
		
		$email=$_POST['username'];
		$password=$_POST['password'];
		
		
		$adminUrl="$baseUrl"."index.php/rest/V1/integration/customer/token";
		
		$ch = curl_init();
		$data = array("username" => "$email", "password" => "$password");                                                                    
		$data_string = json_encode($data);                       
		$ch = curl_init($adminUrl); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);       

		$token = curl_exec($ch);
		$token=  json_decode($token); 
		

		$headers = array("Authorization: Bearer ".$token);
			 

			$requestUrl='http://18.219.227.98/index.php/rest/V1/customers/me';
			 
			$ch = curl_init($requestUrl);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
	        $result1=json_decode($result);
			
			
			
	
		
		if(!empty($result1->id) && isset($result1->id)) {
			
		   /*Starts Fetch User Result */
			
		   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
	       $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	       $connection = $resource->getConnection();
	       $tableName = $resource->getTableName('customer_entity'); //gives table name with prefix
	 
	       $sql = "Select entity_id,email, firstname, lastname, cust_phone, cust_address, session_status FROM ". $tableName." Where email = '$email'";
           $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
			
    		/*Finish Fetch User Result */	
			
			
			
		$data[]=array('error'=>'false','message'=>'You have successfully logged','status_code'=>'200','data'=>$result);
		
		
		    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$tableName = $resource->getTableName('customer_entity'); //gives table name with prefix
			//Update Data into table
			$sql = "Update " . $tableName . " Set session_status = 1 where email= '$email'";
			
			//echo"<BR>==".$sql;
			$connection->query($sql);
		
		
		
		} else {
        $data[]=array('error'=>'true','message'=>'You did not sign in correctly or your account is temporarily disabled','status_code'=>'404');
        }		
		
		unset($data['username']);
		unset($data['password']); 

		echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	

    }
}
