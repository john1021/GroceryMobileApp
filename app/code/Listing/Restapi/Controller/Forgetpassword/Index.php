<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Forgetpassword;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
   public function execute()
    {
	//Authentication rest API magento2.Please change url accordingly your url
	$post = $this->getRequest()->getPostValue();
	$cust_email=$post['cust_email'];
	
	$adminUrl='http://18.219.227.98/index.php/rest/V1/integration/admin/token';
	
	$ch = curl_init();
	
	$data = array("username" => "Raj", "password" => "mani123!!!");    
	
	$data_string = json_encode($data);       
	
	$ch = curl_init($adminUrl); 
	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);   
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);
	
	$token=  json_decode($token); 

	$customerData = [
    "email"=> "$cust_email",
    "template" => "email_reset", 
    "websiteId" => 1
    ];
	
	
    /*Starts Custom Code Either Email Exists or Not */	
	$email_id=$cust_email;
    $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
        ->get('Magento\Framework\App\ResourceConnection');
    $connection= $this->_resources->getConnection();
    $select = $connection->select()
        ->from(
            ['o' =>  $this->_resources->getTableName('customer_entity')]
        )->where('o.email=?',$email_id);
    $result = $connection->fetchAll ($select);
    $total=count($result);
	/*Finish Custom Code Either Email Exists or Not */
	
	$requestUrl='http://18.219.227.98/index.php/rest/V1/customers/password';
	
	//$cust_email="test@yahoo.com";
	
	
	
	if(!empty($cust_email) && isset($cust_email)) {
		
	if($total==1) {
		
    $ch = curl_init();
	$ch = curl_init($requestUrl);	
	//curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");   
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
	$result = curl_exec($ch);
	$result=  json_decode($result);		
	
	$data[]=array('error'=>'false','message'=>"If there is an account associated with $cust_email you will receive an email with a link to reset your password.",'status_code'=>'200','data'=>$result);
	
	} else {
	$data[]=array('error'=>'true','message'=>"There is no account associated with $cust_email, Please write correct email",'status_code'=>'404');
		
	} } else {
	$data[]=array('error'=>'true','message'=>"Please write your correct email",'status_code'=>'404');
	}	
	
	
	//print_r($result);
	//echo '<pre>';print_r($result);
	
	
	
	unset($data['username']);
	unset($data['password']); 
	
	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);		
	
    }
}
