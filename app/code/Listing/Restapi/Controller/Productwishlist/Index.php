<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Productwishlist;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		

    $post = $this->getRequest()->getPostValue();
	
	  $product_id=$_POST['product_id'];
	  $user_id=$_POST['user_id'];
	 
	
	
	if(!empty($_POST['product_id'])){
		
	 $product_id=$_POST['product_id'];
	// $user_id="";
	
	}
	
	if(!empty($_POST['user_id'])){
		
	  $user_id=$_POST['user_id'];
	 //echo"<BR>==".$product_id="no";
	
	}
	
	if(!empty($_POST['product_id']) && !empty($_POST['user_id'])){
		
	$product_id=$_POST['product_id'];
	 $user_id=$_POST['user_id'];
	
	
	}
	
	 //if(empty($user_id)) { echo">>>>>>>>";}
	 //	exit();
	
	
	
	
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
	$baseUrl = $storeManager->getStore()->getBaseUrl();	
	
	$adminUrl="$baseUrl"."index.php/rest/V1/integration/admin/token";
	
	$ch = curl_init();
	$data = array("username" => "admin", "password" => "admin123!@#$");
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);
	$token=  json_decode($token); 
	//exit();

	//echo $token;  
	//Use above token into header
	$headers = array("Authorization: Bearer $token"); 
	
	
	/*Starst Query To Update Phone & Address */
		
	  $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
	  $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	  $connection = $resource->getConnection();
	  $tableName1 = $resource->getTableName('wishlist_item'); //gives table name with prefix
	  $tableName2 = $resource->getTableName('wishlist');
	 
	  if(!empty($product_id) && empty($user_id))  {
		  
         $sql = "select * 
	         from $tableName1 t1 , $tableName2 t2
             where t1.wishlist_id= t2.wishlist_id and t1.product_id=$product_id";
			 
	    $result = $connection->fetchAll($sql); 
		$data[]=array('error'=>'false','message'=>'WishList Details','status_code'=>'200','data'=>$result);
		  
	   } else if(empty($product_id) && !empty($user_id))  {	 

         $sql = "select * 
	             from $tableName1 t1 , $tableName2 t2
                 where t1.wishlist_id= t2.wishlist_id and t2.customer_id=$user_id";
			 
	    $result = $connection->fetchAll($sql); 
		$data[]=array('error'=>'false','message'=>'WishList Details','status_code'=>'200','data'=>$result);
	   
	   
	   } else {
		  
         $sql = "select * 
	             from $tableName1 t1 , $tableName2 t2
                 where t1.wishlist_id= t2.wishlist_id and t2.customer_id=$user_id and t1.product_id=$product_id";
			 
	    $result = $connection->fetchAll($sql); 
		$data[]=array('error'=>'false','message'=>'WishList Details','status_code'=>'200','data'=>$result);
		  
		
       }		

	/*Starst Query To Update Phone & Address */	
		
	unset($data['username']);
	unset($data['password']); 

	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	

        
    }
}
