<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Faq;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
 public function execute()
    {


		
		$adminUrl='http://18.219.227.98/index.php/rest/V1/integration/admin/token';
		$ch = curl_init();
		$data = array("username" => "Raj", "password" => "mani123!!");                                                                    
		$data_string = json_encode($data);                       
		$ch = curl_init($adminUrl); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);       

		$token = curl_exec($ch);
		$token=  json_decode($token); 

			$faq1=array("Que1: Lorem Ipsum is simply dummy text"=>"Ans1:Lorem Ipsum is simply dummy text");
			$faq2=array("Que2:Lorem Ipsum is simply dummy text"=>"Ans2:Lorem Ipsum is simply dummy text");
			$faq3=array("Que3:Lorem Ipsum is simply dummy text"=>"Ans3:Lorem Ipsum is simply dummy text");
			$faq4=array("Que4:Lorem Ipsum is simply dummy text"=>"Ans4:Lorem Ipsum is simply dummy text");
			$faq5=array("Que5:Lorem Ipsum is simply dummy text"=>"Ans5:Lorem Ipsum is simply dummy text");
			
		  //$result[]=array();
		  $result=array_merge($faq1,$faq2, $faq3, $faq4, $faq5);
		  
		/*  echo"<pre>";
		  print_r(array_merge($faq1,$faq2, $faq3, $faq4, $faq5));
	     exit();*/
		
	   // $data[]=array('error'=>'false','message'=>'FAQ','status_code'=>'200','data1'=>$faq1,'data2'=>$faq2,'data3'=>$faq3);
	   //$data[]=array('error'=>'false','message'=>'FAQ','status_code'=>'200','data'=>array_merge($faq1,$faq2, $faq3, $faq4, $faq5));

	   
	      $result = [
				[
				  "Que:"   => "Lorem Ipsum is simply dummy text",
				  "Ans:" => "Lorem Ipsum is simply dummy text"
				],
				[
				  "Que:"   => "Lorem Ipsum is simply dummy text",
				  "Ans:" => "Lorem Ipsum is simply dummy text"
				],
				[
				  "Que:"   => "Lorem Ipsum is simply dummy text",
				  "Ans:" => "Lorem Ipsum is simply dummy text"
				],
				[
				  "Que:"   => "Lorem Ipsum is simply dummy text",
				  "Ans:" => "Lorem Ipsum is simply dummy text"
				],
				[
				  "Que:"   => "Lorem Ipsum is simply dummy text",
				  "Ans:" => "Lorem Ipsum is simply dummy text"
				]
			  ];
	
	
			//echo json_encode($list);
	   
	   
	    $data[]=array('error'=>'false','message'=>'FAQ','status_code'=>'200','data'=>$result);

	   
		unset($data['username']);
		unset($data['password']); 
		echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	
		

    }
}
