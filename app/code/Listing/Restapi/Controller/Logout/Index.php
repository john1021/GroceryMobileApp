<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Logout;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
 public function execute()
    {

	    $post = $this->getRequest()->getPostValue();
		
        //$categoryId=$product_sku=$this->getRequest()->getParam('id');
		
		$user_id=$_POST['user_id'];
		//$password=$_POST['password'];
		
		/*echo"<PRE>";
		print_r($_POST);
	    echo"</PRE>";
		exit();*/
		
		$adminUrl='http://18.219.227.98/index.php/rest/V1/integration/admin/token';
		$ch = curl_init();
		$data = array("username" => "Raj", "password" => "mani123!!");                                                                    
		$data_string = json_encode($data);                       
		$ch = curl_init($adminUrl); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);       

		$token = curl_exec($ch);
		$token=  json_decode($token); 
		//exit();
		

		    /*$headers = array("Authorization: Bearer ".$token);
			 

			$requestUrl='http://18.219.227.98/index.php/rest/V1/customers/me';
			 
			$ch = curl_init($requestUrl);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);*/
			
			
			
		
		//if(!empty($result->id) && isset($result->id)) {
		$data[]=array('error'=>'false','message'=>'You have logged out successfully','status_code'=>'200');
		//} else {
        //$data[]=array('error'=>'true','message'=>'You did not sign in correctly or your account is temporarily disabled','status_code'=>'404');
        //}		
		
		unset($data['username']);
		unset($data['password']); 

		echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	

    }
}
