<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Cmspage;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
    
	/* Starts Process For Base URL */
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $baseUrl = $storeManager->getStore()->getBaseUrl();	
	/* Finish Process For Base URL */
	
	$post = $this->getRequest()->getPostValue();
	$page_id=$_POST['page_id'];
	//exit();
	
	$adminUrl="$baseUrl"."index.php/rest/V1/integration/admin/token";
	
	$ch = curl_init();
	$data = array("username" => "Raj", "password" => "mani123!!!");                                                                    
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);
	$token=  json_decode($token); 
	//exit();

	$headers = array("Authorization: Bearer $token"); 

    //$page_id=4;
	
	
	if(!empty($page_id) && isset($page_id) && $page_id==5 ) {
    
	$requestUrl = "$baseUrl"."index.php/rest/V1/cmsPage/$page_id";
	
	  $ch = curl_init();
	  $ch = curl_init($requestUrl);	
	  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	  $result = curl_exec($ch);
	  $result=  json_decode($result);	
	
	$data[]=array('error'=>'false','message'=>'FAQ','status_code'=>'200','data'=>$result);
	
	}else if(!empty($page_id) && isset($page_id) && $page_id==6 ) {
    
	  $requestUrl = "$baseUrl"."index.php/rest/V1/cmsPage/$page_id";
	  $ch = curl_init();
	  $ch = curl_init($requestUrl);	
	  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	  $result = curl_exec($ch);
	  $result=  json_decode($result);	
	
	$data[]=array('error'=>'false','message'=>'Privacy Policy','status_code'=>'200','data'=>$result);
	
	}else if(!empty($page_id) && isset($page_id) && $page_id==7 ) {
    
	  $requestUrl = "$baseUrl"."index.php/rest/V1/cmsPage/$page_id";
	  $ch = curl_init();
	  $ch = curl_init($requestUrl);	
	  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	  $result = curl_exec($ch);
	  $result=  json_decode($result);	
	
      $data[]=array('error'=>'false','message'=>'Refund Policy','status_code'=>'200','data'=>$result);

	}else if(!empty($page_id) && isset($page_id) && $page_id==8 ) {
    
	  $requestUrl = "$baseUrl"."index.php/rest/V1/cmsPage/$page_id";
	  $ch = curl_init();
	  $ch = curl_init($requestUrl);	
	  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	  $result = curl_exec($ch);
	  $result=  json_decode($result);	
	
      $data[]=array('error'=>'false','message'=>'About Us','status_code'=>'200','data'=>$result);

	
    }else{
		
	$data[]=array('error'=>'true','message'=>'Please enter correct Page ID','status_code'=>'400');	
	
	}	
	 
	  
	
 	  
		
	  unset($data['username']);
	  unset($data['password']); 

	  echo json_encode($data[0], JSON_UNESCAPED_SLASHES);		

		
    }
}
