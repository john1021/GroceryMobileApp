<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Productlistingfilter;

class Index extends \Magento\Framework\App\Action\Action
{

	
	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
    /*Starts Process For Base URL */
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $baseUrl = $storeManager->getStore()->getBaseUrl();	
	/*Finish Process For Base URL */
	//$cat_id=$product_sku=$this->getRequest()->getParam('id');
	$cat_id=$_POST['cat_id'];
	$filter_value=$_POST['filter_value'];
	$sort_value=$_POST['sort_value'];
	
	$page_size=$_POST['page_size'];
	$current_page=$_POST['current_page'];
	$start_price=$_POST['start_price'];
	$end_price=$_POST['end_price'];
	
	/*Starts Process For Category Name */
    $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $object_manager = $_objectManager->create('Magento\Catalog\Model\Category')->load($cat_id);
	$data[]=array();
	$data=$object_manager->getData();
	$data_count=count($data);
	

	$cat_name=$data['name'];
	//exit();
    /*Finish Process For Category Name */
	
	$adminUrl="$baseUrl"."index.php/rest/V1/integration/admin/token";
	
	$ch = curl_init();
	//$data = array("username" => "Raj", "password" => "mani123!!!");
$data = array("username" => "admin", "password" => "admin123!@#$");	
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);

	$token=  json_decode($token); 
	
	$headers = array("Authorization: Bearer $token"); 
	
	if(!empty($cat_id) && !empty($filter_value) && !empty($sort_value)) {
	
  
	if(!empty($start_price) && !empty($end_price)) {
		$requestUrl="$baseUrl"."index.php/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=".$cat_id."&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][condition_type]=eq&"."searchCriteria[filterGroups][2][filters][0][field]=price&searchCriteria[filterGroups][2][filters][0][value]=".$start_price."&searchCriteria[filterGroups][2][filters][0][condition_type]=gteq&searchCriteria[filterGroups][3][filters][0][field]=price&searchCriteria[filterGroups][3][filters][0][value]=".$end_price."&searchCriteria[filterGroups][3][filters][0][condition_type]=lteq"."&"."searchCriteria[sortOrders][0][field]=".$filter_value."&searchCriteria[sortOrders][0][direction]=".$sort_value."&searchCriteria[pageSize]=".$page_size."&searchCriteria[currentPage]=".$current_page;
		} else {
	     $requestUrl="$baseUrl"."index.php/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=".$cat_id."&"."searchCriteria[sortOrders][0][field]=".$filter_value."&searchCriteria[sortOrders][0][direction]=".$sort_value."&searchCriteria[pageSize]=".$page_size."&searchCriteria[currentPage]=".$current_page;
		}	
	
	} else {

	$requestUrl="$baseUrl"."index.php/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=".$cat_id."&"."searchCriteria[pageSize]=".$page_size."&searchCriteria[currentPage]=".$current_page;

    } 		
	
	
//$requestUrl="$baseUrl"."index.php/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=".$cat_id."&searchCriteria[filterGroups][1][filters][0][field]=status&searchCriteria[filterGroups][1][filters][0][value]=1&searchCriteria[filterGroups][1][filters][0][condition_type]=eq&"."searchCriteria[filter_groups][2][filters][0][field]=manufacturer&searchCriteria[filter_groups][2][filters][0][value]="."Buffet"."&searchCriteria[filter_groups][2][filters][0][condition_type]=eq&"."searchCriteria[filterGroups][3][filters][0][field]=price&searchCriteria[filterGroups][3][filters][0][value]=".$start_price."&searchCriteria[filterGroups][3][filters][0][condition_type]=gteq&searchCriteria[filterGroups][4][filters][0][field]=price&searchCriteria[filterGroups][4][filters][0][value]=".$end_price."&searchCriteria[filterGroups][4][filters][0][condition_type]=lteq"."&"."searchCriteria[sortOrders][0][field]=".$filter_value."&searchCriteria[sortOrders][0][direction]=".$sort_value."&searchCriteria[pageSize]=".$page_size."&searchCriteria[currentPage]=".$current_page;
	
	
    $ch = curl_init();
	$ch = curl_init($requestUrl);	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	$result = curl_exec($ch);
	$result=  json_decode($result);	

	//$data[]=array('error'=>'false','message'=>"$cat_name",'status_code'=>'200','data'=>$result);
	

	if($data_count==29){
	$data[]=array('error'=>'false','message'=>"$cat_name",'status_code'=>'200','data'=>$result);
	}else{
	$data[]=array('error'=>'true','message'=>"Please enter required field",'status_code'=>'400');	
	}	
		
		unset($data['username']);
	    unset($data['password']); 

	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);		

		/* 
		echo"<pre>";
		print_r($result);
		echo"</pre>";
		*/
		
    }
}
