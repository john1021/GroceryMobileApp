<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Changepassword;

class Index extends \Magento\Framework\App\Action\Action
{

	
	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
	

	protected $_customerRepoInterface;
	

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
   

		public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Encryption\Encryptor $encryptor,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry
		) 
		{
			parent::__construct($context);
			$this->_customerRepository = $customerRepository;
			$this->_encryptor          = $encryptor;
			$this->_customerRegistry   = $customerRegistry;
		  }

		/************Update  Part ********/

		public function execute()
		  {
			 //echo"<BR>Testing";
			 //exit();
				$username=$_POST['username'];		
				$newpassword=$_POST['newpassword'];	
				$newpassword_confirm=$_POST['newpassword_confirm'];	

               //$username="raj.k230@gmail.com";

               //$newpassword="john123!!";
               //$newpassword_confirm="john123!!"; 	


              $result_pass = [
				[
				  "UserName:"   => "$username",
				  "New Password:" => "$newpassword_confirm"
				]
				
			  ];			   


			   /*Starts Fetch User Result */
				
			   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			   $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			   $connection = $resource->getConnection();
			   $tableName = $resource->getTableName('customer_entity'); //gives table name with prefix
		 
			   $sql = "Select * FROM ". $tableName." Where email = '$username'";
			   $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
				
			   $totalcount=count($result);
			   //exit();
				
			   /*Finish Fetch User Result */	
			 
			   if(($totalcount==1) && ($newpassword==$newpassword_confirm)){
				   
				$customer = $this->_customerRepository->get("raj.k230@gmail.com");
				/* Logic for validation hash from old website here */
				$passwordHash = $this->_encryptor->getHash("$newpassword", true);
				$customerSecure = $this->_customerRegistry->retrieveSecureData($customer->getId());
				$customerSecure->setRpToken(null);
				$customerSecure->setRpTokenCreatedAt(null);
				$customerSecure->setPasswordHash($passwordHash);
				$this->_customerRepository->save($customer, $passwordHash);   
				   
				$data[]=array('error'=>'false','message'=>'Your password has been changed successfully','status_code'=>'200','data'=>$result_pass );
		
				 
				} else if($newpassword!=$newpassword_confirm) { 
				  
				$data[]=array('error'=>'true','message'=>'Please fill confirm password as password','status_code'=>'404');
       	 
				} else {
				  
				  $data[]=array('error'=>'true','message'=>'Please fill required fields','status_code'=>'404');
       	 
				
				}
				
				unset($data['username']);
		        unset($data['password']); 
 
		        echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	

			
          }
}
