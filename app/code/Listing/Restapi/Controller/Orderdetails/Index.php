<?php
/**
 ***********************************************************
 * Copyright © 2015 Listingcommerce. All rights reserved.  *
 ***********************************************************
 */
namespace Listing\Restapi\Controller\Orderdetails;
class Index extends \Magento\Framework\App\Action\Action
{
	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;
    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;
    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
	  
	  
	    $post = $this->getRequest()->getPostValue();
		$product_sku_id=$_POST['sku_id'];
		$customer_id=$_POST['customer_id'];
	  
	  /*Starts For Admin Login Process*/
	    $adminUrl='http://18.219.227.98/index.php/rest/V1/integration/admin/token';
	
		$ch = curl_init();
		$data = array("username" => "Raj", "password" => "mani123!!!");                                                                    
		$data_string = json_encode($data);                       
		$ch = curl_init($adminUrl); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);
	    $token_admin = curl_exec($ch);
		$token_admin=  json_decode($token_admin); 
		$headers = array("Authorization: Bearer ".$token_admin);
	 /*Finish For Admin Login Process*/	

	 /*Starts Cart Empty Process*/
	
	$customerData = ['customer_id' => $customer_id ];
	$ch = curl_init("http://18.219.227.98/index.php/rest/V1/guest-carts");
	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token_admin)));

	$result = curl_exec($ch);

	$quote_id = json_decode($result, 1);
	
	$productData = [
    'cart_item' => [
        'quote_id' => $quote_id,
        'sku' => "$product_sku_id",
        'qty' => 1
    ]];

	                                                     
	$ch = curl_init("http://18.219.227.98/index.php/rest/V1/guest-carts/$quote_id/items");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productData));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token_admin)));

	$result = curl_exec($ch);
	$result = json_decode($result, 1);
	
	
	
	
	/* Starts Shipping Inoformation */
		$Shippinginfo = [
		'addressInformation'=>[
		
		'shippingAddress' => [
				'region' => 'MH',
				'region_id' => 0,
				'country_id' => 'IN',
				'street': [
					'Chakala,Kalyan (e)'
				],
				'company' => 'abc',
				'telephone' => '9891140527',
				'postcode' => '12223',
				'city' => 'Mumbai',
				'country_id' => 'IN',
				'firstname'=> 'Sameer',
				'lastname'=> 'Sawant',
				'email'=> 'paul@qooar.com',
				'prefix'=> 'address_',
				'region_code'=> 'MH',
				'sameAsBilling'=>1
					
		 ]
		 
		'billingAddress' => [
				'region' => 'MH',
				'region_id' => 0,
				'country_id' => 'IN',
				'street': [
					'Chakala,Kalyan (e)'
				],
				'company' => 'abc',
				'telephone' => '9891140527',
				'postcode' => '12223',
				'city' => 'Mumbai',
				'country_id' => 'IN',
				'firstname'=> 'Sameer',
				'lastname'=> 'Sawant',
				'email'=> 'paul@qooar.com',
				'prefix'=> 'address_',
				'region_code'=> 'MH'
				
		 ]
		  'shipping_method_code' => 'flatrate',
		  'shipping_carrier_code'=> 'flatrate'
		
		]];
		
		
		$ch = curl_init("http://18.219.227.98/index.php/rest/V1/guest-carts/$quote_id/shipping-information");
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($Shippinginfo));
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token_admin)));

		$result = curl_exec($ch);
		
		$result = json_decode($result, 1);
		
		echo"<BR>==".$result;
		
		print_r($result);
		
		exit();
	/* Finish Shipping Inoformation */
	
	
	

	if(empty($result)) {
		
	$data[]=array('error'=>'true','message'=>'Currency not found','status_code'=>'404','data'=>$result);
	
	} else {
		
	$data[]=array('error'=>'false','message'=>'Cart Details','status_code'=>'200','data'=>$result);
	
	}
	
	unset($data['username']);
	unset($data['password']); 
	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	
	
    }
}
