<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Slider;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
    //echo"Testing";
	//exit();
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    /* Get base url with index When index.php enable at Url */
    $baseUrl = $storeManager->getStore()->getBaseUrl();	
	$mediaUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);


	$categoryId=$product_sku=$this->getRequest()->getParam('id');	
	//exit();
		
		
	$adminUrl="$baseUrl"."index.php/rest/V1/integration/admin/token";
	
	$ch = curl_init();
	//$data = array("username" => "Raj", "password" => "mani123!!!");    
    $data = array("username" => "admin", "password" => "admin123!@#$");	
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);
	$token=  json_decode($token); 

	$headers = array("Authorization: Bearer $token"); 

	  

	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$connection = $resource->getConnection();
	$tableName = $resource->getTableName('ws_slide'); //gives table name with prefix
		 
		//Select Data from table
		
		$sql = "Select content FROM " . $tableName;
		$result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
		
		//print_r($result);
		//exit();
		
		$first_slider=$mediaUrl.$result[0]['content'];
		$second_slider=$mediaUrl.$result[1]['content'];
		$third_slider=$mediaUrl.$result[2]['content'];
		$fourth_slider=$mediaUrl.$result[3]['content'];
		
		
		$slider_list=$first_slider." ".$second_slider." ".$third_slider." ".$fourth_slider;
		
		$slider_list = explode(" ",$slider_list);
		
	
	     $data[]=array('error'=>'false','message'=>'Product List','status_code'=>'200','data'=>$slider_list);
		
		
		 unset($data['username']);
	     unset($data['password']); 

	     echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	
        
    }
}
