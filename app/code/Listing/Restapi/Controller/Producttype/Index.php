<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Producttype;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		//$this->resultPage = $this->resultPageFactory->create();  
		//return $this->resultPage;
      //Authentication rest API magento2.Please change url accordingly your url
	$adminUrl='http://18.219.227.98/index.php/rest/V1/integration/admin/token';
	
	$ch = curl_init();
	$data = array("username" => "Raj", "password" => "mani123!!!");                                                                    
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);
	$token=  json_decode($token); 
	//exit();

	//echo $token;  
	//Use above token into header
	$headers = array("Authorization: Bearer $token"); 

	
	 
	 $requestUrl='http://18.219.227.98/index.php/rest//V1/products/types';



	 //Please note 24-MB01 is sku
	 
		$ch = curl_init();
		$ch = curl_init($requestUrl);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
		$result = curl_exec($ch);
		$result=  json_decode($result);	

        //echo"<BR>===".count($result);		
		
		if(empty($result)) {
		$data[]=array('error'=>'true','message'=>'Product Type not found','status_code'=>'404','data'=>$result);
		} else {
		$data[]=array('error'=>'false','message'=>'Product Type','status_code'=>'200','data'=>$result);
		}

		unset($data['username']);
	    unset($data['password']); 

		echo json_encode($data[0], JSON_UNESCAPED_SLASHES);		

	/* 
	echo"<pre>";
	print_r($result);
	echo"</pre>";
	*/		
		
		
		
		
        
    }
}
