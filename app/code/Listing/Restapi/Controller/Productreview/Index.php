<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Productreview;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
class Index extends \Magento\Framework\App\Action\Action
{
	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;
    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;
    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
     
		  \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
         $this->_reviewFactory = $reviewFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_storeManager = $storeManager;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		
		
		$post = $this->getRequest()->getPostValue();
	 
		 //print_r($post);
		// exit();

		$product_id=$_POST['product_id'];
		$review_title=$_POST['review_title'];
		$review_detail=$_POST['review_detail'];
		$reviewer_name=$_POST['reviewer_name'];
		
		
		
		
        $productId = $product_id;//product id you set accordingly
        $reviewFinalData['ratings'][1] = 5;
        $reviewFinalData['ratings'][2] = 5;
        $reviewFinalData['ratings'][3] = 5;
        $reviewFinalData['nickname'] = $reviewer_name;
		$nickname=$reviewFinalData['nickname'];
        $reviewFinalData['title'] = $review_title;
        $reviewFinalData['detail'] = $review_detail;
        $review = $this->_reviewFactory->create()->setData($reviewFinalData);
        $review->unsetData('review_id');
        $review->setEntityId($review->getEntityIdByCode(\Magento\Review\Model\Review::ENTITY_PRODUCT_CODE))
            ->setEntityPkValue($productId)
            ->setStatusId(\Magento\Review\Model\Review::STATUS_APPROVED)//By default set approved
            ->setStoreId($this->_storeManager->getStore()->getId())
            ->setStores([$this->_storeManager->getStore()->getId()])
            ->save();
 
        foreach ($reviewFinalData['ratings'] as $ratingId => $optionId) {
            $this->_ratingFactory->create()
                ->setRatingId($ratingId)
                ->setReviewId($review->getId())
                ->addOptionVote($optionId, $productId);
        }
 
        $review->aggregate();
		
		
		/*Starts Fetch User Result */
		
	   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
	   $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	   $connection = $resource->getConnection();
	  
 	   $tableName1 = $resource->getTableName('review_detail'); //gives table name with prefix
	   
	   $tableName2 = $resource->getTableName('review'); //gives table name with prefix

	   $sql = "select t1.title, t1.detail, t1.nickname,t2.entity_pk_value,t2.review_id
			 from $tableName1 t1, $tableName2 t2
			 where t1.review_id = t2.review_id and t2.entity_pk_value=$product_id and t1.nickname='".$reviewer_name."'";
	   $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
	   
	  

		$total=count($result);
		
	 /*Finish Fetch User Result */	

	    if($total>1) {
	   $data[]=array('error'=>'true','message'=>"$nickname Review Detail",'status_code'=>'200','data'=>$result);
        } else {
		$data[]=array('error'=>'false','message'=>"Please fill required fields",'status_code'=>'404');	
		}	
		
		
	unset($data['username']);
	unset($data['password']); 
	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	
		
	  
    }
	
	
	
	
	
}
