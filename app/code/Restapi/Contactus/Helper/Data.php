<?php
/**
 * Copyright © 2015 Restapi . All rights reserved.
 */
namespace Restapi\Contactus\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

	/**
     * @param \Magento\Framework\App\Helper\Context $context
     */
	public function __construct(\Magento\Framework\App\Helper\Context $context
	) {
		parent::__construct($context);
	}
}