<?php
/**
 *
 * Copyright © 2015 Restapicommerce. All rights reserved.
 */
namespace Restapi\Contactus\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		/*echo"<BR>=====";
		$this->resultPage = $this->resultPageFactory->create();  
		return $this->resultPage;
		exit();*/
		$post = $this->getRequest()->getPostValue();
		    $name=$_POST['name'];
			$email=$_POST['email'];
		    $telephone=$_POST['telephone'];
			$comment=$_POST['message'];
		//print_r($_POST);
			//exit();
		if(!empty($name) && !empty($email) && !empty($telephone)) {
			
			 $name=$_POST['name'];
			$email=$_POST['email'];
		    $telephone=$_POST['telephone'];
			$comment=$_POST['message'];
			
			$to = 'rajkumars@henote.net';
			$subject = 'Conatct Us Enquiry';
			$from = $email;
			
			$cust=$email;
			$subject_cust = 'Conatct Us Enquiry';
			$from_cust = 'rajkumars@henote.net';
			 
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			 
			// Create email headers
			$headers .= 'From: '.$from."\r\n".
				'Reply-To: '.$from."\r\n" .
				'X-Mailer: PHP/' . phpversion();
			 
			// Compose a simple HTML email message
			$message = "<html><body>";
			$message .= "<h1 style='color:#000;'>Contact Us Details</h1>";
			$message .= "<p style='color:#000;font-size:18px;'>Name: $name</p>";
			$message .= "<p style='color:#000;font-size:18px;'>Email: $email</p>";
			$message .= "<p style='color:#000;font-size:18px;'>Email: $telephone</p>";
			$message .= "<p style='color:#000;font-size:18px;'>Message: $comment</p>";
			$message .= "</body></html>";
			
			
			/// To send HTML mail, the Content-type header must be set
			$headers_cust  = 'MIME-Version: 1.0' . "\r\n";
			$headers_cust .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			// Create email headers
			$headers_cust .= 'From: '.$from_cust."\r\n".
				'Reply-To: '.$from_cust."\r\n" .
				'X-Mailer: PHP/' . phpversion();
			
			$message_cust = "<html><body>";
			//$message_cust .= "<p style='float:left;font-size:18px;'>Hello $name,</p>";
			$message_cust .= "\n";
			$message_cust .= "<p style='font-size:18px;'>Thank You for contact us, Our Team will soon contact to you.</p>";
			$message_cust .= "\n";
			$message_cust .= "<p style='float:left;font-size:18px;'>Thanking You</p>";
			$message_cust .= "\n";
			$message_cust .= "<p style='float:left;font-size:18px;'>Team Grocery</p>";
			$message_cust .= "</body></html>";
			
			
			 
			// Sending email
			if(mail($to, $subject, $message, $headers) && mail($cust, $subject_cust, $message_cust, $headers_cust)){
				//echo 'Your mail has been sent successfully.';
	    	 $data[]=array('error'=>'false','message'=>'Thank You for contact us, Our Team will soon contact to you.','status_code'=>'200');
			 
	
			} else{
				$data[]=array('error'=>'true','message'=>'Please enter required fields','status_code'=>'400');
			}
			
			
			
			
	    } else {
        $data[]=array('error'=>'true','message'=>'Please enter required fields','status_code'=>'400');	
		}
		
	  unset($data['username']);
	  unset($data['password']); 

	  echo json_encode($data[0], JSON_UNESCAPED_SLASHES);		
        
    }
}
